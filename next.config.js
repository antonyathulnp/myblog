module.exports = {
  reactStrictMode: true,
  images: {
    domains: ['res.cloudinary.com','gitlab.com','firebasestorage.googleapis.com','source.unsplash.com'],
  },
  webpack: function (config) {
    config.module.rules.push({test:  /\.md$/, use: 'raw-loader'})
    config.module.rules.push({test: /\.yml$/, use: 'raw-loader'})
    return config
  }
}
