import Head from 'next/head'
import styles from '../styles/Home.module.css'
import Appbar from '../widgets/appbar'
import ActionBar from '../widgets/actionbar'
import React, { useEffect } from 'react'
import About from '../widgets/about'
import Articles from '../widgets/articles'


export default function Home() {
  const [item,setItem]=React.useState(0)

  useEffect(()=>{
    require('../firebase/index').logScreenView('home_page')
  })

  function onItemClicked(index){
    setItem(index)
  }
  return (
    <>
      <Head>
        <title>Athul Antony&apos;s Blog</title>
        <meta name="description" content="Welcome to Athul Antony's blog. Let's discuss some code here." />
        <link rel="icon" href="/favicon.ico" />
        <link rel="icon" href="/favicon.ico" />
        <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png"/>
        <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png"/>
        <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png"/>
        <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png"/>
        <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png"/>
        <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png"/>
        <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png"/>
        <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png"/>
        <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png"/>
        <link rel="icon" type="image/png" sizes="192x192" href="/android-icon-192x192.png"/>
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png"/>
        <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png"/>
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png"/>
        <link rel="manifest" href="/manifest.json"/>
        <meta name="msapplication-TileColor" content="#ffffff"/>
        <meta name="msapplication-TileImage" content="/ms-icon-144x144.png"/>
        <meta name="theme-color" content="#ffffff"/>
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="theme-color" content="#ffffff" />
        <link href="https://fonts.googleapis.com/css2?family=Outfit:wght@500&display=swap" rel="stylesheet"/>
        <link href="https://fonts.googleapis.com/css2?family=Outfit:wght@600&display=swap" rel="stylesheet"/>
        <link href="https://fonts.googleapis.com/css2?family=Outfit:wght@400&display=swap" rel="stylesheet"/>
        <link href="https://fonts.googleapis.com/css2?family=Outfit:wght@300&display=swap" rel="stylesheet"/>
        <link href="https://fonts.googleapis.com/css2?family=Outfit:wght@500&family=Roboto+Slab:wght@300;400;600&display=swap" rel="stylesheet"></link>
      </Head>

      <main >
        <Appbar/>
         <div className={styles.container}>
           <ActionBar onItemClicked={onItemClicked} currentIndex={item}/>

            <div className={styles.content}>
            {
            item==0 ? <Articles/> : <About/>
          }
            </div>
         </div>
    </main>
    </>
  )
}
