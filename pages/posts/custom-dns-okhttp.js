    import React, { useEffect } from 'react'
    import Appbar from '../../widgets/appbar'
    import styles from '../../styles/Slug.module.css'
    import posts from '../../_posts'
    import Image from 'next/image'
    import { isDev } from '../../utils/dev'
    import PostHeader from '../../widgets/post_header'
    import 'highlight.js/styles/androidstudio.css';
    import hljs from 'highlight.js';
    import kotlin from 'highlight.js/lib/languages/kotlin';
    hljs.registerLanguage('kotlin', kotlin);

    const CustomDnsOkhttp = () => {

    const item=posts.filter(item=>  item.slug==="custom-dns-okhttp")[0]

    useEffect(()=>{
        require('../../firebase/index').logScreenView(item.slug)
        hljs.initHighlighting()
    })

    return <>
    <PostHeader title={item.title} description={item.description}/>
    <main>
    <Appbar/>
        <div className={styles.slug_root}>
            <h1>{item.title}</h1>
            <div className={styles.slug_meta}>
            <div className={styles.slug_date}>Athul Antony | {item.date}</div>
            </div>
            <Image src={item.cover} width="640px" height="420px" alt={item.title}  unoptimized={isDev}
                        objectFit="cover"/>
            <div className={styles.p_top_32}>
            OkHttp is a widely used efficient HTTP & HTTP/2 client for Java,Kotlin and Android applications. If you are using Retrofit and Apollo as REST and GraphQL client for your projects, OkHttp is the underlying library used for this purpose. If you are using these libraries without knowing the actual power of OkHttp, I would like to say that you have not yet witnessed the real show yet. Let’s start with OkHttp’s capability on DNS resolutions.
            </div>
            <p>
            So what’s actually DNS resolution? What does it do? And how beneficial it is for Android developers? Let&apos;s have a look.
            </p>

            <h4>What is DNS resolution?</h4>
            <p>
            The Domain Name System (DNS) is like the phonebook of the Internet. Users access information online through domain names, like google.com or android.com. Web browsers interact through Internet Protocol (IP) addresses. DNS translates domain names to IP addresses so browsers can load Internet resources
            </p>

            <p>
            The process of DNS resolution involves converting a hostname (such as www.example.com) into a computer-friendly IP address (such as 192.168.1.1). An IP address is given to each device on the Internet, and that address is necessary to find the appropriate Internet device - like a street address is used to find a particular home. When a user wants to load a webpage, a translation must occur between what a user types into their web browser (example.com) and the machine-friendly address necessary to locate the example.com webpage.
            </p>

            <p>
            This is actually a simple definition for DNS resolution. So basically, if you look for a domain on the internet, your request will first go to some DNS servers to get the IP for the domain. This is called resolution. After the resolution happens, your device or browser will try to connect with the IP to further proceed with the request.  You can learn more about this  <a href="https://www.cloudflare.com/en-in/learning/dns/what-is-dns/" target="_blank" rel="noreferrer"> here </a>
            </p>

            <h4>How is it important for Android devs?</h4>
            <p>
            There are some few things you need to take care of if your app makes network calls to some external servers. I have come across some of the issues which are related to network issues. I will share it here.
            </p>

            <p>
            Here is the first scenario, one day one of my app’s users came back and told he is facing very slow application performance. During our investigation, we couldn’t find anything wrong with our codebase. He was facing a slow internet. 
            </p>
            <p>
            As an initial step, We have asked him to try switching to some other network provider and test it. His reply was unexpected. He said that he is able to watch youtube videos and use other apps without any slowness. But only our app is slow for him. 
            </p>

            <p>
            This made us get back into further investigation. Finally we found the culprit. The real issue was with his network’s DNS servers. Those DNS servers were very slow or failing in resolution in his geo location. There is still one question pending. How do other app’s gain better performance then? We did further investigation and concluded that those applications tried to do custom DNS resolving with some external DNS servers. There are a lot of public DNS servers available which may be more performant than your network’s normal DNS server. Here is the <a href="https://developers.google.com/speed/public-dns" target="_blank" rel="noreferrer">link</a> to Google’s public DNS server  
            </p>

            <p>
            OkHttp by default uses device DNS resolution class which was pretty slow for that user. So it uses the underlying OS’ DNS resolution mechanism.
            </p>

            <h4>Is it possible to implement our own custom DNS?</h4>

            <p>
            Yes it is possible. We can tell OkHttp to use custom DNS with this few lines of code. 
            </p>
            <pre>
            <code className="kotlin">
            {
    `
    class DnsSelector : Dns { 
                    
        fun lookup(hostname: String): List<InetAddress> { 
            //start dns lookup here
        } 
    }

    // add DnsSelector instance to your OkHttp Builder class

    val client = OkHttpClient.Builder()
                .dns(DnsSelector())
                .build()
                
                `
            }
            </code>
            </pre>

            <p>
            Now inside <span className={styles.markup}>lookup()</span>  we can implement our own DNS resolution code. What we have done was, we implemented an additional DNS resolution using Google DNS and Cloudflare DNS. So if system DNS fails to resolve, we catch it and try to resolve it with other external DNS resolvers. (Google DNS we have used, since it gave more reliability in my use case) 
            </p>

            <pre>
            <code className="kotlin">
            {
                `
    class DnsSelector : Dns { 
                    
        fun lookup(hostname: String): List<InetAddress> { 
            try{
                return System.DNS.lookup(hostname)
            }catch(UnknownHostException e){
                return lookupUsingGoogleDns(hostname)
            }
        } 
    }
                `
            }
            </code>
            </pre>

            <p>
            This has solved the user&apos;s P0 issue, which was he is not able to load contents from external server.Now the issue he faces is slow response time. This was because the system DNS was taking some time to resolve (Fails after that). Primarily we had set `System.DNS` as our source. This caused a delay in network calls. So in order to solve this issue, we came up with another trick. 
            </p>

            <p>
            When the user opens the app we try to log DNS resolution time with all the providers and we have made an algorithm to improve the order of DNS providers used in the `lookup()`. So the fastest answering DNS server will be used for further calls. This way, the user will stick on to the fastest DNS server all the time.
            </p>

            <h4>Issues with non IPv6 supported domains</h4>
            <p>
            Internet Protocol version 6 (IPv6) is the most recent version of the Internet Protocol (IP), the communications protocol that provides an identification and location system for computers on networks and routes traffic across the Internet. It was succeeded by IPv4. Most of the domains in the world are still not fully compatible with IPv6. So how does this affect your app’s network performance? 
            </p>

            <p>
            If your WiFi/4G/5G network supports both IPv6 and IPv4, it will give two different types of address during DNS resolution.  So what happens if your domain fully does not support v6? The okhttp will try to connect with the first ipv6 addresses from the DNS resolution address. This will run into timeout or connection failure after sometime. Then after v6 failure only okhttp will use ipv4 addresses to connect to server. This will successfully connect.
            </p>

            <p>
            This introduces a waiting time for IPv6 connection attempts to fail. If you set a large connectTime for your OkHttp instance, the entire network request-response time will also impact. So you can remove this waiting time to connect with ipv6 by below code. This will reduce your request response time.
            </p>

            <pre>
            <code className="kotlin">
            {
                `
    class DnsSelector : Dns { 
                    
        fun lookup(hostname: String): List<InetAddress> { 

             return Dns.SYSTEM.lookup(hostname)
                .filter { Inet4Address::class.java.isInstance(it)
        } 
    }
                `
            }
            </code>
            </pre>

            <p>
            The logic is simple, the DNS lookup result will omit all the IPv6 addresses and it will return only IPv4 addresses. Hence OkHttp will try to connect with only IPv4 addresses. 
            </p>

            <p>
            More read : There is something called the  <a href="https://en.wikipedia.org/wiki/Happy_Eyeballs" rel="noreferrer" target="_blank">
            Happy Eyeballs algorithm </a> which will try to connect with both IPv6 and IPv4 addresses at the sametime inorder to reduce this issue. Currently browsers like Google Chrome have implemented these algorithms for faster browse experience. I guess OkHttp currently doesn’t have plans to implement these in the future releases.
            </p>
        </div>
    </main>
        
    </>
    }

    export default CustomDnsOkhttp