const posts=[
    {
        title: "Custom DNS resolution with OkHttp",
        description:"Blog post talks about how to do custom DNS resolution with OkHttp and it also describes about the edge cases while implementing it.",
        thumbnail:"https://source.unsplash.com/QQ2d5naZee0/640x426",
        cover:"https://source.unsplash.com/QQ2d5naZee0/1600x900",
        slug:"custom-dns-okhttp",
        date:"22 Nov 2021",
        tags:['Android']
    }
]


export default posts