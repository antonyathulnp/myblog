import { initializeApp } from "firebase/app";
import { getAnalytics,logEvent} from "firebase/analytics";

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyDKUCFHBpnN5EenOmlzM5jmqfpzuPAX0B0",
  authDomain: "athulblog-ca03e.firebaseapp.com",
  projectId: "athulblog-ca03e",
  storageBucket: "athulblog-ca03e.appspot.com",
  messagingSenderId: "964404652817",
  appId: "1:964404652817:web:dba84b5877854e5b40fab0",
  measurementId: "G-YRBPH67HXW"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);


function logFEvent(event){
    logEvent(analytics,event)
}

function logScreenView(screenName){
    logEvent(analytics, 'screen_view', {
        firebase_screen: screenName
      });
}
module.exports=  {
    logFEvent:logFEvent,
    logScreenView:logScreenView
}