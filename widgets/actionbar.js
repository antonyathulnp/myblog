import React from 'react'
import styles from './../styles/Actionbar.module.css'

const ActionBar=(props)=>{

    return <div className={styles.actionbar}>
    <div className={props.currentIndex===0 ? styles.action_button_active : styles.action_button} onClick={()=>props.onItemClicked(0)}>Posts</div>
    <div className={props.currentIndex===1 ? styles.action_button_active : styles.action_button} onClick={()=>props.onItemClicked(1)}>About Me</div>
</div>
}

export default ActionBar