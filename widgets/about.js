import React from 'react'
import styles from './../styles/About.module.css'

const About =()=>{
    return <div className={styles.about_root}>
        <span>Hey, I am a diligent software engineer with 5+ years experience in application development. 
            I focus on building softwares that aim to be solve the challenging problems in the world.
            I am currently working at an online e-commerce app as a software engineer. Previously worked as Senior product engineer at <a  target="_blank" href="https://entri.app">Entri.app </a>
        </span>
       
       <span>I have more than 5+ years of experience in building native Android applications using Kotlin and as well as Java.
           MVVM,Coroutines,Jetpack Essentials are the common things in my toolbox. 
       </span>
       <span>
          Other than Android,  I also have good experience in ReactJS,Node.js,MongoDB (MERN). I have written 
            2 MERN stack apps and it is running production with a good userbase. You can reach them through 
            <a href="https://humtum.app" target="_blank"> HumTum</a> & <a target="_blank" href="https://quickprofit.in">QuickProfit </a>
       </span>
       <span>To know more about me and my works, You can connect me through below social and contact links</span>

       <ul>
           <li><a target="_blank"   href="https://github.com/athulantonynp">GitHub</a></li>
           <li><a  target="_blank" href="https://www.linkedin.com/in/athul-antony-np-5242b4104/">LinkedIn</a></li>
           <li><a target="_blank"  href="https://twitter.com/athulantonynp">Twitter</a></li>
           <li><a target="_blank" href="https://web.whatsapp.com/send?phone=917829144145">WhatsApp</a></li>
       </ul>
    </div>
}

export default About