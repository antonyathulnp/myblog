import React from 'react'
import styles from './../styles/Articles.module.css'
import posts from '../_posts'
import { useRouter } from 'next/router'
import Image from 'next/image'
import { isDev } from '../utils/dev'


const Articles=()=>{
    const router=useRouter()

    function onArticleClicked(article){
        router.push(`/posts/${article.slug}`)
    }

    return <div className={styles.articles_main}>
        {
            posts.map(item=>{
                return <div className={styles.article_item} key={item.title} onClick={()=> onArticleClicked(item)}>
                    <div className={styles.article_image}>
                    <Image src={item.thumbnail} width="400px" height="180px" alt={item.title}  unoptimized={isDev}
                    objectFit="cover"/>
                    </div>
                    <div className={styles.article_data}>
                    <div className={styles.article_title}>{item.title}</div>
                    <div className={styles.article_desc}>{item.description}</div>

                    <div className={styles.article_date}>{item.date}</div>
                    </div>
                     </div>
            })
        }
    </div>
}

export default Articles