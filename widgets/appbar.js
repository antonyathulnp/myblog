import React, { useEffect } from 'react'
import styles from './../styles/Appbar.module.css'
import Image from 'next/image'
import { useRouter } from 'next/router'

const Appbar=(props)=>{
  const router=useRouter()

    function onHomeClick(){
        router.push('/')
    }

    return <div className={styles.navbar}>
        <span className={styles.dot}  onClick={()=>onHomeClick()} >
        <Image src={"https://gitlab.com/antonyathulnp/qp_assets/-/raw/master/logo.svg"} alt="me" width="36" height="36" />
        </span>
        <div className={styles.title} onClick={()=>onHomeClick()}>Athul&apos;s Blog</div>
  </div>
}

export default Appbar